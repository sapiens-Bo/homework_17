﻿#include <iostream>
#include <cmath>

class Vector 
{
private:
	double x;
	double y;
	double z;

public:
	Vector()
		:x(0), y(0), z(0)
	{}

	Vector(double x_, double y_, double z_)
		:x(x_), y(y_), z(z_)
	{}

	double getX() 
	{
		return x;
	}
	double getY() 
	{
		return y;
	}
	double getZ() 
	{
		return z;
	}

	double getLength() 
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}

};

class Quadra
{
public:
	Quadra() 
		:a(0), b(0), c(0), d(0)
	{
	}

	Quadra(double a_)
		:a(a_), b(a_), c(a_), d(a_)
	{
	}

	Quadra(double width, double height)
		:a(width), b(height), c(width), d(height)
	{
	}

	Quadra(double a_, double b_, double c_, double d_)
		:a(a_), b(b_), c(c_), d(d_)
	{
	}
	

	double getA() { return a; }
	double getB() { return b; }
	double getC() { return c; }
	double getD() { return d; }

	double getSquare()
	{
		return a + b + c + d;
	}
private:
	double a;
	double b;
	double c;
	double d;
};

int main()
{
	Vector vec1(6, -3, 4);
	std::cout << vec1.getLength() << " " << vec1.getX() << " " << vec1.getY() << " " << vec1.getZ() << std::endl;
}